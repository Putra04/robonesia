-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 06 Nov 2020 pada 16.57
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `robonesia_id`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` varchar(255) NOT NULL,
  `posted` datetime NOT NULL,
  `image` blob NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id`, `judul`, `isi`, `posted`, `image`, `penulis`, `views`) VALUES
(7, 'Teknologi', '<p>aaaaaaaaaaa</p>', '0000-00-00 00:00:00', 0x64643364653535616463663964636534396633616137616265346361316334642e6a7067, 'jajang', 0),
(8, 'Robotika', '<p>abcdefg</p>', '0000-00-00 00:00:00', 0x30343466383939376535653732326633646166363235336539643534613538612e706e67, 'yanyan', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri_foto`
--

CREATE TABLE `galeri_foto` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `posted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deskripsi` varchar(255) NOT NULL,
  `image` blob NOT NULL,
  `views` int(11) NOT NULL,
  `penulis` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `galeri_foto`
--

INSERT INTO `galeri_foto` (`id`, `judul`, `posted`, `deskripsi`, `image`, `views`, `penulis`) VALUES
(4, 'TEST ISI Dari Web', '2020-10-21 09:22:02', 'OK', 0x31356235623666303966666161643735646435343833323565336139613237302e706e67, 0, 'puput'),
(5, 'Test Upload Foto', '2020-11-04 15:02:03', 'owrait', 0x31313432666562336234353930633566613266313335336466356362396338662e706e67, 0, 'Luthfi Aziz');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri_video`
--

CREATE TABLE `galeri_video` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `posted` timestamp NOT NULL DEFAULT current_timestamp(),
  `deskripsi` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `views` int(11) NOT NULL,
  `penulis` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `galeri_video`
--

INSERT INTO `galeri_video` (`id`, `judul`, `posted`, `deskripsi`, `video`, `views`, `penulis`) VALUES
(1, 'Teknologi', '2020-10-23 07:06:57', 'asefqwefeqwfeqf', 'xxx', 0, 'pipip');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kontak` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama`, `alamat`, `kontak`, `email`) VALUES
(7, 'dampleng', 'yordania', '0812345678', 'uyep@gmail.com'),
(8, 'a', 'yordania', '0812345678', 'uyeasdfadfp@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kontak` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `member`
--

INSERT INTO `member` (`id`, `nama`, `alamat`, `kontak`, `email`) VALUES
(2, 'bn', 'qqq', '1234', 'uyep@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `proflie_robonesia`
--

CREATE TABLE `proflie_robonesia` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `visi` varchar(255) NOT NULL,
  `misi` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `proflie_robonesia`
--

INSERT INTO `proflie_robonesia` (`id`, `logo`, `deskripsi`, `contact`, `visi`, `misi`, `nama`, `alamat`) VALUES
(2, '', 'Robonesia adalah sebuah startup yang menyediakan mendia pembelajaran robotika berupa kegiatan ekstrakulikuler, privat dan bimbingan belajar robotika yang tersebar di lebih dari 40 sekolah di kota Bandung dan lebih dari 1200 siswa di Jawa Barat. Robonesia ', '085399629154 (tel)  /  085721133032 (WA) .    e-mail : robonesia.rumahrobot@gmail.com', '“Menjadi startup yang kompeten dan kompetitif di Indonesia yang dapat memberikan wawasan luas kepada anak bangsa tentang dunia robotika”', '1. Membuka wawasan anak bangsa mngenai dunia robotika agar terlahir banyak ilmuwan bidang robotik di masa depan yang akan mengharumkan nama bangsa Indonesia di dunia internasional. \r\n2. Menyelenggarakan pendidikan robotik yang professional, berkualitas da', 'ROBONESIA', 'Dago, Bandung. Jawa Barat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `saran`
--

CREATE TABLE `saran` (
  `id` int(11) NOT NULL,
  `pengirim` varchar(255) NOT NULL,
  `isi` varchar(255) NOT NULL,
  `posted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `saran`
--

INSERT INTO `saran` (`id`, `pengirim`, `isi`, `posted`, `keterangan`) VALUES
(1, 'Herman', 'Saran dan Kritikan', '2020-09-22 17:00:00', 'Belum Dibaca'),
(3, 'darsa', 'Saran', '2020-11-04 15:31:53', 'Sudah Dibaca');

-- --------------------------------------------------------

--
-- Struktur dari tabel `seminar`
--

CREATE TABLE `seminar` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kontak` varchar(255) NOT NULL,
  `email` int(255) NOT NULL,
  `tanggal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `seminar`
--

INSERT INTO `seminar` (`id`, `nama`, `alamat`, `kontak`, `email`, `tanggal`) VALUES
(5, 'uuuuuu', 'ppppp', '453535345345', 0, '11/18/2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jk` varchar(255) NOT NULL,
  `kontak` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` blob NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `jk`, `kontak`, `email`, `image`, `role`) VALUES
(3, '', '', 'uyaaa', 'laki laki', '0812345678', 'uyep@gmail.com', 0x31343663313331393139653530353836616261333239343465653463393561652e6a7067, 0),
(4, '', '', 'neng', 'Perempuan', '0897654343354667', 'nengyytt@gmail.com', 0x30373962663130326361313533313365363232613535346361393936646165632e706e67, 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `galeri_foto`
--
ALTER TABLE `galeri_foto`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `galeri_video`
--
ALTER TABLE `galeri_video`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `proflie_robonesia`
--
ALTER TABLE `proflie_robonesia`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `seminar`
--
ALTER TABLE `seminar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `galeri_foto`
--
ALTER TABLE `galeri_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `galeri_video`
--
ALTER TABLE `galeri_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `proflie_robonesia`
--
ALTER TABLE `proflie_robonesia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `saran`
--
ALTER TABLE `saran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `seminar`
--
ALTER TABLE `seminar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
