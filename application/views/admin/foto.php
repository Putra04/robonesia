<div id="page-wrapper">
	<div class="page-title" style="margin-top: -20px;">
		<div class="title_left">
			<h3>Galerry</h3>
			<br>
		</div>
	</div>
	<?php 
	$data=$this->session->flashdata('sukses');
	if($data!=""){ ?>
		<div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
	<?php } ?>

	<?php 
	$data2=$this->session->flashdata('error');
	if($data2!=""){ ?>
		<div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
	<?php } ?>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header" style="line-height:70px;">
						<?php if($foto->num_rows() > 0)  {?>
							<div class="box-body" style="overflow: auto;">
								<table id="example1" class="table table-bordered" style="font-size:12px; text-align: justify;">
									<thead>
										<tr style="background: #fff;">
											<th width="5"><center>No</center></th>
											<th width="200px"><center>Judul</center></th>
											<th width="100"><center>Gambar</center></th>
											<th width="80"><center>Posted</center></th>
											<th width="120"><center>Deskripsi</center></th>
											<th width="80"><center>Aksi</center></th>
										</tr>
									</thead>
									<?php $no=1;
									foreach($foto->result_array() as $i):
										$id=$i['id'];
										$judul=$i['judul'];
										$posted=$i['posted'];
										$deskripsi=$i['deskripsi'];
										$image = $i['image'];
										$views = $i['views'];						
										?>
										<tbody>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $judul ?></td>
												<?php if($image == "")  {?>
													<td>Gambar Tidak Ditemukan</td>
												<?php } else {?>
													<td>
														<center><img width="200" src="<?php echo base_url(). 'assets/img/galeri/'.$image.''; ?>" class="img-thumbnail" ></center>
													</td>
												<?php } ?>
												<td><?php echo $posted ?></td>
												<td><?php echo $deskripsi ?></td>
												<td align="center">
													<a href="" data-toggle="modal" data-target="#modal-editfoto<?=$id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
													<a href="<?php echo site_url('c_admin/deletefoto/'.$id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
												</td>
											</tr>
										</tbody>
									<?php endforeach;?>
								</table>
							</div>

						<?php } else {?>
							<center>
								<div style="margin: 50px;">
									<img width="200" src="<?php echo base_url(). 'assets/img/utility/empty.png'; ?>">
									<h3>Data Kosong</h3>
								</div>
							</center>
						<?php } ?>
					</div>
				</div>
				<a class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modal-tambahfoto"><span class="fa fa-plus"></span> Upload Foto</a>
			</div>
		</div>
	</section>

</div>

<!-- Modal Tambah Foto -->
<div class="modal fade" id="modal-tambahfoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
				<h4 class="modal-title" id="myModalLabel"> Upload</h4>
			</div>

			<form class="form-horizontal" action="<?php echo site_url('c_admin/addfoto'); ?>" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="modal-body">
						<input name="id" type="hidden" value="">
						<div class="form-group">
							<label>Judul</label>
							<input class="form-control" name="judul" type="text" placeholder="Input judul Foto" value="" required>
						</div>
						<div class="form-group">
							<label>Gambar</label>
							<input class="form-control" type="file" name="image">
						</div>
						<div class="form-group">
							<label>Penulis</label>
							<input class="form-control" name="penulis" type="text" placeholder="Take By .." value="" required>
						</div>
						<div class="form-group">
							<label>Deskripsi</label><br>
							<textarea  name="deskripsi" style="width: 100%" placeholder="Tuliskan Deskripsi Foto"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-warning btn-flat" id="simpan">Simpan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div> 
<!-- Akhir Modal Tambah Foto -->

<!-- Modal Edit -->
<?php $no=1;
foreach($foto->result_array() as $i):
	$id=$i['id'];
	$judul=$i['judul'];
	$deskripsi=$i['deskripsi'];
	$posted=$i['posted'];
	$penulis=$i['penulis'];
	$image = $i['image'];
	?>
	<div class="modal fade" id="modal-editfoto<?=$id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
					<h4 class="modal-title" id="myModalLabel"> Update Foto</h4>
				</div>

				<form class="form-horizontal" action="<?php echo site_url('c_admin/editfoto'); ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="modal-body">
							<input name="id" type="hidden" value="">
							<input type="hidden" readonly value="<?=$id;?>" name="id" class="form-control" >
							<input type="hidden" readonly value="<?=$image;?>" name="oldimage" class="form-control" >
							<div class="form-group">
								<label>Judul</label>
								<input class="form-control" name="judul" type="text" placeholder="Input judul artikel" value="<?=$judul;?>" required>
							</div>
							<div class="row">
								<div>
									<?php if($image == "" || $image == NULL) {?>
										<td>Gambar Tidak Ditemukan</td>
									<?php } else { ?>
										<img width="200" height="200" src="<?php echo base_url(). 'assets/img/galeri/'.$image.''; ?>" class="img-thumbnail" >
									<?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label>Gambar</label>
								<input class="form-control" type="file" name="image" value="<?=$image;?>">
							</div>
							<div class="form-group">
								<label>Penulis</label>
								<input class="form-control" name="penulis" type="text" placeholder="Penulis artikel" value="<?=$penulis;?>" required>
							</div>
							<div class="form-group">
								<label>Deskripsi</label><br>
								<textarea  name="deskripsi" style="width: 100%" placeholder="Tuliskan deskripsi"><?php echo $deskripsi;?></textarea>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div> 
<?php endforeach;?>
<!-- Akhir modal edit -->