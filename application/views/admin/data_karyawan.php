<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
     <h3><center>Data Karyawan</center></h3>
     <h3><small>Data Karyawan Robonesia</small></h3>
   </div>
 </div>
 <?php 
 $data=$this->session->flashdata('sukses');
 if($data!=""){ ?>
  <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>

<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
  <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
       <?php if($data_karyawan->num_rows() > 0) { ?>
         <div class="box-header">
          <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
            <thead>
              <tr style="background: #fff;">
                <th width="220"><center>Nama Karyawan</th>
                  <th width="300"><center>alamat</center></th>
                  <th width="110"><center>kontak</th>
                    <th><center>email</th>
                      <th width="200"><center>Aksi</th>
                      </tr>
                    </thead>
                    <?php
                    foreach($data_karyawan->result_array() as $a):
                      $id=$a['id'];
                      $nama=$a['nama'];
                      $alamat=$a['alamat'];
                      $kontak=$a['kontak'];
                      $email=$a['email'];
                      ?>
                      <tbody>
                        <tr>
                          <td><?php echo $nama ?></td>
                          <td><?php echo $alamat ?></td>
                          <td><?php echo $kontak ?></td>
                          <td><?php echo $email ?></td>

                          <td align="center">
                            <a href="" data-toggle="modal" data-target="#modal-editkaryawan<?=$id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="<?php echo site_url('c_admin/deletekaryawan/'.$id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>

                          </td>
                        </tr>
                      </tbody>
                    <?php endforeach;?>
                  </table>
                  <!-- <center>Data Kosong</center> -->
                </div>
              <?php  } else {?>
                <div style="margin-top: 100px;">
                  <center><img width="200" height="200" src="<?php echo base_url(). 'assets/img/utility/datakosong.png'; ?>" ></center>
                  <h2><small><center>Data Kosong</center></small></h2>
                </div>
              <?php } ?>
              <a class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modal-tambahkaryawan"><span class="fa fa-plus"></span> Tambah data</a>

              <!-- Modal Tambah Artikel -->
              <div class="modal fade" id="modal-tambahkaryawan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-warning">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                      <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
                    </div>

                    <form class="form-horizontal" action="<?php echo site_url('c_admin/addkaryawan'); ?>" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        <div class="modal-body">
                          <input name="id" type="hidden" value="">
                          <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" name="nama" type="text" placeholder="Input nama karyawan" value="" required>
                          </div>
                          <div class="form-group">
                            <label>Alamat</label>
                            <input class="form-control" name="alamat" type="text" placeholder="Input alamat karyawan" value="" required>
                          </div>
                          <div class="form-group">
                            <label>kontak</label>
                            <input class="form-control" name="kontak" type="text" placeholder="kontak karyawan" value="" required>
                          </div>
                          <div class="form-group">
                            <label>email</label>
                            <input class="form-control" name="email" type="text" placeholder="email karyawan" value="" required>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning btn-flat" id="simpan">Simpan</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div> 
              <!-- Akhir Modal Tambah Artikel -->

              <!-- Modal Edit -->
              <?php $no=1;
              foreach($data_karyawan->result_array() as $i):
                $id=$i['id'];
                $nama=$i['nama'];
                $alamat=$i['alamat'];
                $kontak=$i['kontak'];
                $email=$i['email'];
                ?>
                <div class="modal fade" id="modal-editkaryawan<?=$id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header bg-warning">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel"> Update Karyawan</h4>
                      </div>

                      <form class="form-horizontal" action="<?php echo site_url('c_admin/editkaryawan'); ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                          <div class="modal-body">
                            <input name="id" type="hidden" value="">
                            <input type="hidden" readonly value="<?=$id;?>" name="id" class="form-control" >
                            <div class="form-group">
                              <label>Nama</label>
                              <input class="form-control" name="nama" type="text" placeholder="Input nama karyawan" value="<?=$nama;?>" required>
                            </div>
                            <div class="form-group">
                              <label>alamat</label>
                              <input class="form-control" name="alamat" type="text" placeholder="Input Alamat Karyawan" value="<?=$alamat;?>" required>
                            </div>
                            <div class="form-group">
                              <label>Kontak</label>
                              <input class="form-control" name="kontak" type="text" placeholder="Kontak" value="<?=$kontak;?>" required>
                            </div>
                            <div class="form-group">
                              <label>email</label>
                              <input class="form-control" name="email" type="text" placeholder="Email" value="<?=$email;?>" required>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div> 
              <?php endforeach;?>
              <!-- Akhir modal edit -->

            </div>
          </div>
        </div>
      </section>
    </div>