<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
     <h3><center>User Account</center></h3>
     <h3><small>Data user Robonesia</small></h3>
   </div>
 </div>
 <?php 
 $data=$this->session->flashdata('sukses');
 if($data!=""){ ?>
  <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>

<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
  <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
       <?php if($user->num_rows() > 0) { ?>
         <div class="box-header">
          <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
            <thead>
              <tr style="background: #fff;">
                <th width="220"><center>Nama User</th>
                  <th width="300"><center>Jenis Kelamin</center></th>
                  <th width="110"><center>kontak</th>
                    <th><center>email</th>
                    <th width="100"><center>Image</th>
                      <th width="200"><center>Aksi</th>
                      </tr>
                    </thead>
                    <?php
                    foreach($user->result_array() as $a):
                      $id=$a['id'];
                      $nama=$a['nama'];
                      $jk=$a['jk'];
                      $kontak=$a['kontak'];
                      $email=$a['email'];
                      $image=$a['image'];
                      ?>
                      <tbody>
                        <tr>
                          <td><?php echo $nama ?></td>
                          <td><?php echo $jk ?></td>
                          <td><?php echo $kontak ?></td>
                          <td><?php echo $email ?></td>
                          <?php if($image == "")  {?>
													<td>Gambar Tidak Ditemukan</td>
												<?php } else {?>
													<td>
														<center><img width="200" src="<?php echo base_url(). 'assets/img/artikel/'.$image.''; ?>" class="img-thumbnail" ></center>
													</td>
												<?php } ?>

                          <td align="center">
                            <a href="" data-toggle="modal" data-target="#modal-edituser<?=$id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="<?php echo site_url('c_admin/deleteuser/'.$id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>

                          </td>
                        </tr>
                      </tbody>
                    <?php endforeach;?>
                  </table>
                  <!-- <center>Data Kosong</center> -->
                </div>
              <?php  } else {?>
                <div style="margin-top: 100px;">
                  <center><img width="200" height="200" src="<?php echo base_url(). 'assets/img/utility/datakosong.png'; ?>" ></center>
                  <h2><small><center>Data Kosong</center></small></h2>
                </div>
              <?php } ?>
              <a class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modal-tambahuser"><span class="fa fa-plus"></span> Tambah data</a>

              <!-- Modal Tambah User -->
              <div class="modal fade" id="modal-tambahuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-warning">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                      <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
                    </div>

                    <form class="form-horizontal" action="<?php echo site_url('c_admin/adduser'); ?>" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        <div class="modal-body">
                          <input name="id" type="hidden" value="">
                          <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" name="nama" type="text" placeholder="Input Nama User" value="" required>
                          </div>
                          <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <input class="form-control" name="jk" type="text" placeholder="Jenis Kelamin" value="" required>
                          </div>
                          <div class="form-group">
                            <label>kontak</label>
                            <input class="form-control" name="kontak" type="text" placeholder="Kontak User" value="" required>
                          </div>
                          <div class="form-group">
                            <label>email</label>
                            <input class="form-control" name="email" type="text" placeholder="Email User" value="" required>
                          </div>
                          <div class="form-group">
							<label>Foto</label>
							<input class="form-control" type="file" name="image">
						</div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning btn-flat" id="simpan">Simpan</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div> 
              <!-- Akhir Modal Tambah Artikel -->

              <!-- Modal Edit -->
              <?php $no=1;
              foreach($user->result_array() as $i):
                $id=$i['id'];
                $username=$i['username'];
                $password=$i['password'];
                      $nama=$i['nama'];
                      $jk=$i['jk'];
                      $kontak=$i['kontak'];
                      $email=$i['email'];
                      $image=$i['image'];
                      ?>
                <div class="modal fade" id="modal-edituser<?=$id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header bg-warning">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel"> Update Data User</h4>
                      </div>

                      <form class="form-horizontal" action="<?php echo site_url('c_admin/edituser'); ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                          <div class="modal-body">
                            <input name="id" type="hidden" value="">
                            <input type="hidden" readonly value="<?=$id;?>" name="id" class="form-control" >
                            <div class="form-group">
                              <label>Nama</label>
                              <input class="form-control" name="nama" type="text" placeholder="Input nama User" value="<?=$nama;?>" required>
                            </div>
                            <div class="form-group">
                              <label>Jenis Kelamin</label>
                              <input class="form-control" name="jk" type="text" placeholder="Input jenis kelamin" value="<?=$jk;?>" required>
                            </div>
                            <div class="form-group">
                              <label>Kontak</label>
                              <input class="form-control" name="kontak" type="text" placeholder="Kontak" value="<?=$kontak;?>" required>
                            </div>
                            <div class="form-group">
                              <label>email</label>
                              <input class="form-control" name="email" type="text" placeholder="Email" value="<?=$email;?>" required>
                            </div>
                            <div class="form-group">
                              <label>Foto</label>
                              <input class="form-control" name="image" type="file" placeholder="Image" value="<?=$image;?>" required>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div> 
              <?php endforeach;?>
              <!-- Akhir modal edit -->

            </div>
          </div>
        </div>
      </section>
    </div>