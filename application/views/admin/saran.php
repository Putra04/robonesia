<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
     <h3><center>Saran</center></h3>
     <h3><small>Pengaduan, Kritik dan Saran dari user</small></h3>
   </div>
 </div>
 <?php 
 $data=$this->session->flashdata('sukses');
 if($data!=""){ ?>
  <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>

<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
  <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
       <?php if($saran->num_rows() > 0) { ?>
         <div class="box-header">
          <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
            <thead>
              <tr style="background: #fff;">
                <th width="220"><center>Nama Pengirim</th>
                  <th width="300"><center>Isi</center></th>
                  <th width="110"><center>Posted</th>
                    <th><center>Keterangan</th>
                      </tr>
                    </thead>
                    <?php
                    foreach($saran->result_array() as $a):
                      $id=$a['id'];
                      $pengirim=$a['pengirim'];
                      $isi=$a['isi'];
                      $posted=$a['posted'];
                      $keterangan=$a['keterangan'];
                      ?>
                      <tbody>
                        <tr>
                          <td><?php echo $pengirim ?></td>
                          <td><?php echo $isi ?></td>
                          <td><?php echo $posted ?></td>
                          <td><?php echo $keterangan ?></td>
                        </tr>
                      </tbody>
                    <?php endforeach;?>
                  </table>
                  <!-- <center>Data Kosong</center> -->
                </div>
              <?php  } else {?>
                <div style="margin-top: 100px;">
                  <center><img width="200" height="200" src="<?php echo base_url(). 'assets/img/utility/datakosong.png'; ?>" ></center>
                  <h2><small><center>Data Kosong</center></small></h2>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </section>
    </div>