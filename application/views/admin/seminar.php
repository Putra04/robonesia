<div id="page-wrapper">
  <div class="page-title" style="margin-top: -20px;">
    <div class="title_left">
     <h3><center>Pengajuan Seminar</center></h3>
     <h3><small>Data Pengajuan Seminar</small></h3>
   </div>
 </div>
 <?php 
 $data=$this->session->flashdata('sukses');
 if($data!=""){ ?>
  <div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
<?php } ?>

<?php 
$data2=$this->session->flashdata('error');
if($data2!=""){ ?>
  <div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
<?php } ?>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
       <?php if($seminar->num_rows() > 0) { ?>
         <div class="box-header">
          <table id="example1" class="table table-striped table-bordered" style="font-size:12px; text-align: justify;">
            <thead>
              <tr style="background: #fff;">
                <th width="220"><center>Nama Instansi</th>
                  <th width="300"><center>Alamat Instansi</center></th>
                  <th width="110"><center>kontak</th>
                    <th><center>email</th>
                      <th><center>tanggal</th>
                        <th width="200"><center>Aksi</th>
                        </tr>
                      </thead>
                      <?php
                      foreach($seminar->result_array() as $a):
                        $id=$a['id'];
                        $nama=$a['nama'];
                        $alamat=$a['alamat'];
                        $kontak=$a['kontak'];
                        $email=$a['email'];
                        $tanggal=$a['tanggal'];
                        ?>
                        <tbody>
                          <tr>
                            <td><?php echo $nama ?></td>
                            <td><?php echo $alamat ?></td>
                            <td><?php echo $kontak ?></td>
                            <td><?php echo $email ?></td>
                            <td><?php echo $tanggal ?></td>

                            <td align="center">
                              <a href="" data-toggle="modal" data-target="#modal-editseminar<?=$id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                              <a href="<?php echo site_url('c_admin/deleteseminar/'.$id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>

                            </td>
                          </tr>
                        </tbody>
                      <?php endforeach;?>
                    </table>
                    <!-- <center>Data Kosong</center> -->
                  </div>
                <?php  } else {?>
                  <div style="margin-top: 100px;">
                    <center><img width="200" height="200" src="<?php echo base_url(). 'assets/img/utility/datakosong.png'; ?>" ></center>
                    <h2><small><center>Data Kosong</center></small></h2>
                  </div>
                <?php } ?>
                <a class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modal-tambahseminar"><span class="fa fa-plus"></span> Tambah data</a>

                <!-- Modal Tambah Artikel -->
                <div class="modal fade" id="modal-tambahseminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header bg-warning">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel"> Tambah</h4>
                      </div>

                      <form class="form-horizontal" action="<?php echo site_url('c_admin/addseminar'); ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                          <div class="modal-body">
                            <input name="id" type="hidden" value="">
                            <div class="form-group">
                              <label>Nama Instansi</label>
                              <input class="form-control" name="nama" type="text" placeholder="Input Nama Intansi" value="" required>
                            </div>
                            <div class="form-group">
                              <label>Alamat Instansi</label>
                              <input class="form-control" name="alamat" type="text" placeholder="Input Alamat Instansi" value="" required>
                            </div>
                            <div class="form-group">
                              <label>kontak</label>
                              <input class="form-control" name="kontak" type="text" placeholder="kontak" value="" required>
                            </div>
                            <div class="form-group">
                              <label>email</label>
                              <input class="form-control" name="email" type="text" placeholder="email" value="" required>
                            </div>
                            <div class="form-group">
                              <label>Tanggal</label>
                              <input class="form-control" id="datepicker" name="tanggal" type="text" placeholder="tanggal" value="" required autocomplete="off">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-warning btn-flat" id="simpan">Simpan</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div> 
                <!-- Akhir Modal Tambah Artikel -->

                <!-- Modal Edit -->
                <?php $no=1;
                foreach($seminar->result_array() as $i):
                  $id=$i['id'];
                  $nama=$i['nama'];
                  $alamat=$i['alamat'];
                  $kontak=$i['kontak'];
                  $email=$i['email'];
                  $tanggal=$i['tanggal'];
                  ?>
                  <div class="modal fade" id="modal-editseminar<?=$id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header bg-warning">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                          <h4 class="modal-title" id="myModalLabel"> Update Data</h4>
                        </div>

                        <form class="form-horizontal" action="<?php echo site_url('c_admin/editseminar'); ?>" method="post" enctype="multipart/form-data">
                          <div class="modal-body">
                            <div class="modal-body">
                              <input name="id" type="hidden" value="">
                              <input type="hidden" readonly value="<?=$id;?>" name="id" class="form-control" >
                              <div class="form-group">
                                <label>Nama Instansi</label>
                                <input class="form-control" name="nama" type="text" placeholder="Input Nama Instansi" value="<?=$nama;?>" required>
                              </div>
                              <div class="form-group">
                                <label>Alamat Instansi</label>
                                <input class="form-control" name="alamat" type="text" placeholder="Input Alamat Instansi" value="<?=$alamat;?>" required>
                              </div>
                              <div class="form-group">
                                <label>Kontak</label>
                                <input class="form-control" name="kontak" type="text" placeholder="Kontak" value="<?=$kontak;?>" required>
                              </div>
                              <div class="form-group">
                                <label>email</label>
                                <input class="form-control" name="email" type="text" placeholder="Email" value="<?=$email;?>" required>
                              </div>
                              <div class="form-group">
                                <label>Tanggal</label>
                                <input class="form-control" id="datepicker" name="tanggal" type="text" autocomplete="off" placeholder="Tanggal" value="<?=$tanggal;?>" required>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div> 
                <?php endforeach;?>
                <!-- Akhir modal edit -->

              </div>
            </div>
          </div>
        </section>
      </div>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>