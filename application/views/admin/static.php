<!DOCTYPE html>
<html lang="en">
<head>
<?php $this->load->view('admin/parsial/headeradmin'); ?>
<?php $this->load->view('admin/parsial/scriptadmin'); ?>
</head>

<body>
    <div id="wrapper" >
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background-color:rgba(255,255,255,1);  ">
            <div class="navbar-header">
                
                <a class="navbar-brand" href="<?php echo base_url()?>Dashboard">Robonesia</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <!-- /.dropdown -->
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw" style="color: #a67c00"></i> <i class="fa fa-caret-down" style="color: #a67c00"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" style="color: #a67c00"><i class="fa fa-user fa-fw" style="color: #a67c00"></i> Admin Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url() ?>loginadmin/logout" style="color: red"><i class="fa fa-sign-out fa-fw" style="color: red"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation"  style=" background-color:rgba(255,255,255,1);">
                <div class="sidebar-nav navbar-collapse">   
                    <ul class="nav sidemenu" id="side-menu">
                        <!-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" style="color: #a67c00">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </li> -->
                        <li>
                            <a href="<?php echo base_url('c_admin/index');?>" style="color:    #a67c00"><i style="color:   #a67c00" class="fa fa-home fa-fw"></i> Dashboard</a>
                        </li>

                        <li>
                            <a href="#" style="color:   #a67c00"><i class="fa fa-edit" style="color:   #a67c00"></i> Tentang Robonesia<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('c_admin/profile')?>" style="color:   #a67c00"><i class=" fa fa-user" aria-hidden="true" style="color:   #a67c00"> Profile Robonesia</i></a></li>
                                <li>
                                    <a href="<?php echo base_url('c_admin/data_karyawan')?>" style="color:   #a67c00"><i class=" fa fa-file" aria-hidden="true" style="color:   #a67c00"> Data Karyawan</i></a></li>
                                <li>
                                    <a href="<?php echo base_url('c_admin/data_member')?>" style="color:   #a67c00"><i class=" fa fa-file" aria-hidden="true" style="color:   #a67c00"> Data Member</i></a></li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="#" style="color:   #a67c00"><i class="fa fa-folder-open" style="color:   #a67c00"></i> Konten<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('c_admin/artikel')?>" style="color:    #a67c00"><i class="fa fa-upload" style="color:   #a67c00"></i> Artikel</a>
                                </li>
                                    <li><a href="#" style="color:   #a67c00"><i class="fa fa-file" style="color:  #a67c00"></i> Galerry<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_url('c_admin/foto')?>" style="color:    #a67c00"><i class=" fa fa-photo" aria-hidden="true" style="color:   #a67c00"> Foto</i></a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('c_admin/video')?>" style="color:   #a67c00"><i class=" fa fa-file-video-o" aria-hidden="true" style="color:   #a67c00"> Video</i></a>
                                        </li>
                                    </ul>
                                </li>
                                    <!-- /.nav-second-level -->
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="#" style="color:   #a67c00"><i class="fa fa-edit" style="color: #a67c00"></i> Layanan Robonesia<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url('c_admin/seminar')?>" style="color: #a67c00"><i class=" fa fa-book" aria-hidden="true" style="color:   #a67c00"> Pengajuan Seminar</i></a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?php echo base_url('c_admin/saran');?>" style="color:     #a67c00"><i class="fa fa-envelope fa-fw" style="color:   #a67c00"></i> Saran</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('c_admin/user');?>" style="color:  #a67c00"><i class="fa fa-user" style="color:  #a67c00"></i> User Acount</a>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
        </nav>
    </div>
</body>
</html>
