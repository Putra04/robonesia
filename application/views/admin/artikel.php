<div id="page-wrapper">
	<div class="page-title" style="margin-top: -20px;">
		<div class="title_left">
			<h3>Artikel</h3>
		</div>
	</div>
	<?php 
	$data=$this->session->flashdata('sukses');
	if($data!=""){ ?>
		<div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
	<?php } ?>

	<?php 
	$data2=$this->session->flashdata('error');
	if($data2!=""){ ?>
		<div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
	<?php } ?>
	<section class="content">
    <?php if ($artikel->num_rows() > 0){ ?><div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header" style="line-height:70px;">
            <div class="box-body" style="overflow: auto;">
              <table id="example1" class="table table-bordered" style="font-size:12px; text-align: justify;">
                <thead>
                  <tr style="background: #fff;">
                    <th width="5"><center>No</center></th>
                    <th width="200px"><center>Judul</center></th>
                    <th width="100"><center>Gambar</center></th>
                    <th width="80"><center>Posted</center></th>
                    <th width="120"><center>Penulis</center></th>
                    <th width="400"><center>Isi</center></th>
                    <th width="80"><center>Aksi</center></th>
                  </tr>
                </thead>
                <?php $no=1;
                foreach($artikel->result_array() as $i):
                  $id=$i['id'];
                  $judul=$i['judul'];
                  $isi=$i['isi'];
                  $posted=$i['posted'];
                  $penulis=$i['penulis'];
                  $image = $i['image'];
                  $views = $i['views'];
                  ?>
                  <tbody>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $judul ?></td>
                      <?php if($image == "") {?>
                        <td>Gambar Tidak Ditemukan</td>
                      <?php } else {?>
                        <td>
                          <img width="200" src="<?php echo base_url(). 'assets/img/artikel/'.$image.''; ?>" class="img-thumbnail" >
                        </td>
                      <?php } ?>
                      <td><?php echo $posted ?></td>
                      <td><?php echo $penulis ?></td>
                      <td><?php echo $isi ?></td>
                      <td align="center">
                        <a href="" data-toggle="modal" data-target="#modal-editartikel<?=$id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="<?php echo site_url('c_admin/deleteartikel/'.$id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
                      </td>
                    </tr>
                  </tbody>
                <?php endforeach;?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php } else { ?>  
    <div style="margin-top: 100px;">
      <center><img width="200" height="200" src="<?php echo base_url(). 'assets/img/utility/datakosong.png'; ?>" ></center>
      <h2><small><center>Artikel Kosong</center></small></h2>
    </div>
  <?php } ?>

</section>
<a class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modal-tambahartikel"><span class="fa fa-plus"></span> Tambah Artikel</a>
</div>

<!-- Modal Tambah Artikel -->
<div class="modal fade" id="modal-tambahartikel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
				<h4 class="modal-title" id="myModalLabel"> Tambah</h4>
			</div>

			<form class="form-horizontal" action="<?php echo site_url('c_admin/addartikel'); ?>" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="modal-body">
						<input name="id" type="hidden" value="">
						<div class="form-group">
							<label>Judul</label>
							<input class="form-control" name="judul" type="text" placeholder="Input judul artikel" value="" required>
						</div>
						<div class="form-group">
							<label>Gambar</label>
							<input class="form-control" type="file" name="image">
						</div>
						<div class="form-group">
							<label>Penulis</label>
							<input class="form-control" name="penulis" type="text" placeholder="Penulis artikel" value="" required>
						</div>
						<div class="form-group">
							<label>Isi artikel</label><br>
							<textarea  name="isi" style="width: 100%" placeholder="Tuliskan artikel terbaru"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div> 
<!-- Akhir Modal Tambah Artikel -->


<!-- Modal Edit -->
<?php $no=1;
foreach($artikel->result_array() as $i):
	$id=$i['id'];
	$judul=$i['judul'];
	$isi=$i['isi'];
	$posted=$i['posted'];
	$penulis=$i['penulis'];
	$image = $i['image'];
	?>
	<div class="modal fade" id="modal-editartikel<?=$id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-warning">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
					<h4 class="modal-title" id="myModalLabel"> Update artikel</h4>
				</div>

				<form class="form-horizontal" action="<?php echo site_url('c_admin/editartikel'); ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="modal-body">
							<input name="id" type="hidden" value="">
							<input type="hidden" readonly value="<?=$id;?>" name="id" class="form-control" >
							<input type="hidden" readonly value="<?=$image;?>" name="oldimage" class="form-control" >
							<div class="form-group">
								<label>Judul</label>
								<input class="form-control" name="judul" type="text" placeholder="Input judul artikel" value="<?=$judul;?>" required>
							</div>
							<div class="row">
								<div>
									<?php if($image == "" || $image == NULL) {?>
                    <td>Gambar Tidak Ditemukan</td>
                  <?php } else { ?>
                   <img width="200" height="200" src="<?php echo base_url(). 'assets/img/artikel/'.$image.''; ?>" class="img-thumbnail" >
                 <?php } ?>
               </div>
             </div>
             <div class="form-group">
              <label>Gambar</label>
              <input class="form-control" type="file" name="image" value="<?=$image;?>">
            </div>
            <div class="form-group">
              <label>Penulis</label>
              <input class="form-control" name="penulis" type="text" placeholder="Penulis artikel" value="<?=$penulis;?>" required>
            </div>
            <div class="form-group">
              <label>Isi artikel</label><br>
              <textarea  name="isi" style="width: 100%" placeholder="Tuliskan artikel terbaru"><?php echo $isi;?></textarea>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-warning btn-flat" id="simpan">Simpan</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div> 
<?php endforeach;?>
<!-- Akhir modal edit -->


<!-- Script untuk memanggil fungsi seperti word -->
<script type="text/javascript" src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		plugins: [
		"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
		"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"table contextmenu directionality emoticons template textcolor paste textcolor filemanager"
		],

		toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
		toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
		toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",

		menubar: false,
		toolbar_items_size: 'small',
		image_advtab: true,
		style_formats: [
		{title: 'Bold text', inline: 'b'},
		{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
		{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
		{title: 'Example 1', inline: 'span', classes: 'example1'},
		{title: 'Example 2', inline: 'span', classes: 'example2'},
		{title: 'Table styles'},
		{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		],

		templates: [
		{title: 'Test template 1', content: 'Test 1'},
		{title: 'Test template 2', content: 'Test 2'}
		]
	});
</script>
<script>
	$(document).ready(function(){
		$("#myInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#myTable tr").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
	});
</script>
<!-- Akhir Script -->