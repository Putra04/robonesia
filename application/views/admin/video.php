<div id="page-wrapper">
	<div class="page-title" style="margin-top: -20px;">
		<div class="title_left">
			<h2>Galerry</h2>
			<h3><small>Video</small></h3>
			<br>
		</div>
	</div>
	<?php 
	$data=$this->session->flashdata('sukses');
	if($data!=""){ ?>
		<div id="notifikasi" class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Sukses! </strong> <?=$data;?></div>
	<?php } ?>

	<?php 
	$data2=$this->session->flashdata('error');
	if($data2!=""){ ?>
		<div id="notifikasi" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong> Error! </strong> <?=$data2;?></div>
	<?php } ?>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header" style="line-height:70px;">
						<?php if($video->num_rows() > 0)  {?>
							<div class="box-body" style="overflow: auto;">
								<table id="example1" class="table table-bordered" style="font-size:12px; text-align: justify;">
									<thead>
										<tr style="background: #fff;">
											<th width="5"><center>No</center></th>
											<th width="200px"><center>Judul</center></th>
											<th width="100"><center>Link Video</center></th>
											<th width="80"><center>Posted</center></th>
											<th width="120"><center>Deskripsi</center></th>
											<th width="80"><center>Aksi</center></th>
										</tr>
									</thead>
									<?php $no=1;
									foreach($video->result_array() as $i):
										$id=$i['id'];
										$judul=$i['judul'];
										$posted=$i['posted'];
										$deskripsi=$i['deskripsi'];
										$video = $i['video'];
										$views = $i['views'];
										$penulis = $i['penulis'];						
										?>
										<tbody>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $judul ?></td>
												<td><?php echo $video ?></td>
												<td><?php echo $posted ?></td>
												<td><?php echo $deskripsi ?></td>
												<td align="center">
													<a href="" data-toggle="modal" data-target="#modal-editvideo<?=$id;?>" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-edit"></i></a>
													<a href="<?php echo site_url('c_admin/deletevideo/'.$id); ?>" class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-trash"></i></i></a>
												</td>
											</tr>
										</tbody>
									<?php endforeach;?>
								</table>
							</div>

						<?php } else {?>
							<center>
								<div style="margin: 50px;">
									<img width="200" src="<?php echo base_url(). 'assets/img/utility/empty.png'; ?>">
									<h3>Data Kosong</h3>
								</div>
							</center>
						<?php } ?>
					</div>
				</div>
				<a class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modal-tambahvideo"><span class="fa fa-plus"></span> Tambah Video</a>
			</div>
		</div>
	</section>

</div>

<!-- Modal Tambah Foto -->
<div class="modal fade" id="modal-tambahvideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
				<h4 class="modal-title" id="myModalLabel"> Upload</h4>
			</div>

			<form class="form-horizontal" action="<?php echo site_url('c_admin/addvideo'); ?>" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="modal-body">
						<input name="id" type="hidden" value="">
						<div class="form-group">
							<label>Judul</label>
							<input class="form-control" name="judul" type="text" placeholder="Input judul video" value="" required>
						</div>
						<div class="form-group">
							<label>Video</label>
							<input class="form-control" name="video" type="text" placeholder="Link video" value="" required>
						</div>
						<div class="form-group">
							<label>Penulis</label>
							<input class="form-control" name="penulis" type="text" placeholder="Take By .." value="" required>
						</div>
						<div class="form-group">
							<label>Deskripsi</label><br>
							<textarea  name="deskripsi" style="width: 100%" placeholder="Tuliskan Deskripsi Video"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-warning btn-flat" id="simpan">Simpan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div> 
<!-- Akhir Modal Tambah Foto -->
<!-- Modal Edit -->
<?php foreach ($videoo->result_array() as $x):
	$id=$x['id'];
	$judul=$x['judul'];
	$posted=$x['posted'];
	$deskripsi=$x['deskripsi'];
	$video = $x['video'];
	$views = $x['views'];
	$penulis = $x['penulis'];	
	?>
	<div class="modal fade" id="modal-editvideo<?=$id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
					<h4 class="modal-title" id="myModalLabel"> Update video</h4>
				</div>

				<form class="form-horizontal" action="<?php echo site_url('c_admin/editvideo'); ?>" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="modal-body">
							<input name="id" type="hidden" value="">
							<input type="hidden" readonly value="<?=$id;?>" name="id" class="form-control" >
							<div class="form-group">
								<label>Judul</label>
								<input class="form-control" name="judul" type="text" placeholder="Input judul video" value="<?=$judul;?>" required>
							</div>
							<div class="row">
							</div>
							<div class="form-group">
								<label>Video</label>
								<input class="form-control" type="text" name="video" placeholder="link video" value="<?=$video;?>" required>
							</div>
							<div class="form-group">
								<label>Penulis</label>
								<input class="form-control" name="penulis" type="text" placeholder="Penulis" value="<?=$penulis;?>" required>
							</div>
							<div class="form-group">
								<label>Deskripsi</label><br>
								<textarea  name="deskripsi" style="width: 100%" placeholder="Tuliskan deskripsi"><?php echo $deskripsi;?></textarea>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div> 
<?php endforeach ?>
<!-- Akhir modal edit -->

