<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class c_home extends CI_Controller
{
	function __construct(){
    parent::__construct();
    $this->load->model('m_admin');
    }
	public function index(){
		$this->load->view('index');
	}
}