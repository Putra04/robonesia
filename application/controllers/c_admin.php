<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class c_admin extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('m_admin');
	}
	public function index(){
		$data['user'] = $this->m_admin->getlengthuser();
		$data['saran'] = $this->m_admin->getlengthsaran();
		$data['artikel'] = $this->m_admin->getlengthartikel();
		$data['foto'] = $this->m_admin->getlengthfoto();
		// var_dump($data['user']->result_array());
		// exit();
		$this->load->view('admin/static');
		$this->load->view('admin/dashboard',$data);
	}
	public function saran(){
		$data['saran'] = $this->m_admin->getsaran();
		$this->load->view('admin/static');
		$this->load->view('admin/saran',$data);

	}
	public function addsaran(){
		$pengirim = $this->input->post('pengirim');
		$isi = $this->input->post('isi');
		$posted = $this->input->post('posted');
		$keterangan = $this->input->post('keterangan');

		$data =  array(
			'pengirim'=> $pengirim,
			'isi'	  => $isi,
			'posted'     => $posted,
			'keterangan'		  => $keterangan
		);
		$this->m_admin->insertsaran($data);
		redirect('c_admin/saran');
	}
	public function deletesaran($id){
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('c_admin/saran');
		}else{
			$this->m_admin->deletesaran($data,$id);
			redirect('c_admin/saran');
		}
	}
	public function editsaran(){

		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('pengirim', 'pengirim', 'required');

		$pengirim = $this->input->post('pengirim');
		$isi = $this->input->post('isi');
		$keterangan = $this->input->post('keterangan');
		// exit();
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('c_admin/saran');
		}else{
			$data=array( 
				"pengirim"=>$pengirim,
				"isi"=>$isi,
				"posted"=>$posted,
				"keterangan"=>$keterangan,
			);
			$this->m_admin->editsaran($data,$id);
			redirect('c_admin/saran');
		}
	}
	public function transaksi(){
		$data['transaksi'] = $this->m_admin->getsaran();
		$this->load->view('admin/static');
		$this->load->view('admin/transaksi',$data);

	}
	public function user(){
		$data['user'] = $this->m_admin->getuser();
		$this->load->view('admin/static');
		$this->load->view('admin/user',$data);
	}
	public function adduser(){
		$nama = $this->input->post('nama');
		$jk = $this->input->post('jk');
		$kontak = $this->input->post('kontak');
		$email = $this->input->post('email');
		$image = $this->input->post('image');

		if ($image=''){} else{
			$config['upload_path']='./assets/img/artikel';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;


			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image')){
				echo "Menambahkan Data User Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}

			$data =  array(
				'nama'=> $nama,
				'jk'	  => $jk,
				'kontak'     => $kontak,
				'email'     => $email,
				'image'		  => $image
			);
			$this->m_admin->insertuser($data);
			redirect('c_admin/user');
		}
	}
	public function profile(){
		$data['profile'] = $this->m_admin->getprofile();
		$this->load->view('admin/static');
		$this->load->view('admin/profile',$data);
	}
	public function data_karyawan(){
		$data['data_karyawan'] = $this->m_admin->getkaryawan();
		$this->load->view('admin/static');
		$this->load->view('admin/data_karyawan',$data);
	}
	public function addkaryawan(){
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$kontak = $this->input->post('kontak');
		$email = $this->input->post('email');

		$data =  array(
			'nama'=> $nama,
			'alamat'	  => $alamat,
			'kontak'     => $kontak,
			'email'		  => $email
		);
		$this->m_admin->insertkaryawan($data);
		redirect('c_admin/data_karyawan');
	}
	public function deleteuser($id){
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('c_admin/user');
		}else{
			$this->m_admin->deleteuser($data,$id);
			redirect('c_admin/user');
		}
	}
	public function edituser(){

		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');

		$nama = $this->input->post('nama');
		$jk = $this->input->post('jk');
		$kontak = $this->input->post('kontak');
		$email = $this->input->post('email');
		$image = $this->input->post('image');
		$oldimage = $this->input->post('oldimage');
		// var_dump($image);
		// var_dump($oldimage);
		// exit();
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('c_admin/user');
		}else{
			$data=array( 
				"nama"=>$nama,
				"jk"=>$jk,
				"kontak"=>$kontak,
				"email"=>$email,
			);

			if (!empty($_FILES["image"]["name"])){
				$config['upload_path']='./assets/img/artikel';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;


				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('image')){
					echo "Menambahkan User Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['image'] = $image;
					if($image!=NULL){
						$path = './assets/img/user/'.$oldimage.'';
						unlink($path);
					}
				}
			}
			$this->m_admin->edituser($data,$id);
			redirect('c_admin/user');
		}
	}
	public function deletekaryawan($id){
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('c_admin/data_karyawan');
		}else{
			$this->m_admin->deletekaryawan($data,$id);
			redirect('c_admin/data_karyawan');
		}
	}
	public function editkaryawan(){

		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');

		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$kontak = $this->input->post('kontak');
		$email = $this->input->post('email');
		// exit();
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('c_admin/data_karyawan');
		}else{
			$data=array( 
				"nama"=>$nama,
				"alamat"=>$alamat,
				"kontak"=>$kontak,
				"email"=>$email,
			);
			$this->m_admin->editkaryawan($data,$id);
			redirect('c_admin/data_karyawan');
		}
	}
	public function data_member(){
		$data['data_member'] = $this->m_admin->getmember();
		$this->load->view('admin/static');
		$this->load->view('admin/data_member',$data);
	}
	public function addmember(){
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$kontak = $this->input->post('kontak');
		$email = $this->input->post('email');

		$data =  array(
			'nama'=> $nama,
			'alamat'	  => $alamat,
			'kontak'     => $kontak,
			'email'		  => $email
		);
		$this->m_admin->insertmember($data);
		redirect('c_admin/data_member');
	}
	public function editmember(){

		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');

		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$kontak = $this->input->post('kontak');
		$email = $this->input->post('email');
		// exit();
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('c_admin/data_member');
		}else{
			$data=array( 
				"nama"=>$nama,
				"alamat"=>$alamat,
				"kontak"=>$kontak,
				"email"=>$email,
			);
			$this->m_admin->editmember($data,$id);
			redirect('c_admin/data_member');
		}
	}
	public function deletemember($id){
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('c_admin/data_member');
		}else{
			$this->m_admin->deletemember($data,$id);
			redirect('c_admin/data_member');
		}
	}
	public function artikel(){
		$data['artikel'] = $this->m_admin->getartikel();
		$this->load->view('admin/static');
		$this->load->view('admin/artikel',$data);
	}
	public function addartikel(){
		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$penulis = $this->input->post('penulis');
		$image = $this->input->post('image');

		if ($image=''){} else{
			$config['upload_path']='./assets/img/artikel';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;


			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image')){
				echo "Menambahkan Berita Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}

			$data =  array(
				'judul'=> $judul,
				'isi'	  => $isi,
				'penulis'     => $penulis,
				'image'		  => $image
			);
			$this->m_admin->insertartikel($data);
			redirect('c_admin/artikel');
		}
	}

	public function editartikel(){

		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('judul', 'judul', 'required');

		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$penulis = $this->input->post('penulis');
		$image = $this->input->post('image');
		$oldimage = $this->input->post('oldimage');
		// var_dump($image);
		// var_dump($oldimage);
		// exit();
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('c_admin/artikel');
		}else{
			$data=array( 
				"judul"=>$judul,
				"isi"=>$isi,
				"penulis"=>$penulis,
			);

			if (!empty($_FILES["image"]["name"])){
				$config['upload_path']='./assets/img/artikel';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;


				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('image')){
					echo "Menambahkan Berita Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['image'] = $image;
					if($image!=NULL){
						$path = './assets/img/artikel/'.$oldimage.'';
						unlink($path);
					}
				}
			}
			$this->m_admin->editartikel($data,$id);
			redirect('c_admin/artikel');
		}
	}

	public function deleteartikel($id){
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('c_admin/artikel');
		}else{
			$this->m_admin->deleteartikel($data,$id);
			redirect('c_admin/artikel');
		}
	}
	public function foto(){
		$data['foto'] = $this->m_admin->getfoto();
		$this->load->view('admin/static');
		$this->load->view('admin/foto',$data);
	}

	public function deletefoto($id){
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('c_admin/foto');
		}else{
			$this->m_admin->deletefoto($data,$id);
			redirect('c_admin/foto');
		}
	}

	public function addfoto(){
		$judul = $this->input->post('judul');
		$deskripsi = $this->input->post('deskripsi');
		$penulis = $this->input->post('penulis');
		$image = $this->input->post('image');

		if ($image=''){} else{
			$config['upload_path']='./assets/img/galeri';
			$config['allowed_types']='jpg|gif|png|jpeg';
			$config['encrypt_name'] = TRUE;


			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('image')){
				echo "Menambahkan Berita Gagal"; die();
			}
			else{
				$image=$this->upload->data('file_name');
	             //  $config['source_image']='./assets/img/berita'.$image['file_name'];
	             //  $config['new_image']= './assets/img/berita'.$image['file_name']; 
	            	// $config['create_thumb']= FALSE;
	            	// $config['maintain_ratio']= FALSE;
	            	// $config['quality']= '60%';
	            	// $config['width']= 710;
	            	// $config['height']= 460;
			}

			$data =  array(
				'judul'=> $judul,
				'deskripsi'	  => $deskripsi,
				'penulis'     => $penulis,
				'image'		  => $image
			);
			$this->m_admin->insertfoto($data);
			redirect('c_admin/foto');
		}
	}

	public function editfoto(){

		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('judul', 'judul', 'required');

		$judul = $this->input->post('judul');
		$deskripsi = $this->input->post('deskripsi');
		$penulis = $this->input->post('penulis');
		$image = $this->input->post('image');
		$oldimage = $this->input->post('oldimage');
		// var_dump($image);
		// var_dump($oldimage);
		// exit();
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('c_admin/foto');
		}else{
			$data=array( 
				"judul"=>$judul,
				"deskripsi"=>$deskripsi,
				"penulis"=>$penulis,
				"image"=>$image,
			);

			if (!empty($_FILES["image"]["name"])){
				$config['upload_path']='./assets/img/galeri';
				$config['allowed_types']='jpg|gif|png|jpeg';
				$config['encrypt_name'] = TRUE;


				$this->load->library('upload',$config);
				if(!$this->upload->do_upload('image')){
					echo "Menambahkan Berita Gagal"; die();
				}
				else{
					$image=$this->upload->data('file_name');
					$data['image'] = $image;
					if($image!=NULL){
						$path = './assets/img/galeri/'.$oldimage.'';
						unlink($path);
					}
				}
			}
			$this->m_admin->editfoto($data,$id);
			redirect('c_admin/foto');
		}
	}
	public function video(){
		$data['video'] = $this->m_admin->getvideo();
		$data['videoo'] = $this->m_admin->getvideo();
		$this->load->view('admin/static');
		$this->load->view('admin/video',$data);
	}
	public function deletevideo($id){
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('c_admin/video');
		}else{
			$this->m_admin->deletevideo($data,$id);
			redirect('c_admin/video');
		}
	}
	public function addvideo(){
		$judul = $this->input->post('judul');
		$deskripsi = $this->input->post('deskripsi');
		$penulis = $this->input->post('penulis');
		$video = $this->input->post('video');

		$data =  array(
			'judul'=> $judul,
			'deskripsi'	  => $deskripsi,
			'penulis'     => $penulis,
			'video'		  => $video,
		);
		$this->m_admin->insertvideo($data);
		redirect('c_admin/video');
	}
	public function editvideo(){

		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('judul', 'judul', 'required');

		$judul = $this->input->post('judul');
		$deskripsi = $this->input->post('deskripsi');
		$penulis = $this->input->post('penulis');
		$video = $this->input->post('video');
		// var_dump($image);
		// var_dump($oldimage);
		// exit();
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('c_admin/video');
		}else{
			$data=array( 
				"judul"=>$judul,
				"deskripsi"=>$deskripsi,
				"penulis"=>$penulis,
				"video"=>$video,
			);

			$this->m_admin->editvideo($data,$id);
			redirect('c_admin/video');
		}
	}
	public function seminar(){
		$data['seminar'] = $this->m_admin->getseminar();
		$this->load->view('admin/static');
		$this->load->view('admin/seminar',$data);
	}
	public function addseminar(){
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$kontak = $this->input->post('kontak');
		$email = $this->input->post('email');
		$tanggal = $this->input->post('tanggal');
		$data =  array(
			'nama'=> $nama,
			'alamat'	  => $alamat,
			'kontak'     => $kontak,
			'email'		  => $email,
			'tanggal'		  => $tanggal
		);
		$this->m_admin->insertseminar($data);
		redirect('c_admin/seminar');
	}
	public function editseminar(){

		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');

		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$kontak = $this->input->post('kontak');
		$email = $this->input->post('email');
		$tanggal = $this->input->post('tanggal');
		// exit();
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Edit");
			redirect('c_admin/seminar');
		}else{
			$data=array( 
				"nama"=>$nama,
				"alamat"=>$alamat,
				"kontak"=>$kontak,
				"email"=>$email,
				"tanggal"=>$tanggal,
			);
			$this->m_admin->editseminar($data,$id);
			redirect('c_admin/seminar');
		}
	}
	public function deleteseminar($id){
		if($id==""){
			$this->session->set_flashdata('error',"Data Anda Gagal Di Hapus");
			redirect('c_admin/seminar');
		}else{
			$this->m_admin->deleteseminar($data,$id);
			redirect('c_admin/seminar');
		}
	}
}