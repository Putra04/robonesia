<?php
/**
 * 
 */
class m_admin extends CI_Model
{
	public function getsaran(){
		$querysaran = $this->db->query('SELECT * FROM saran');
		return $querysaran;
	}

  public function insertsaran($data){
    $this->db->insert('saran',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Ditambahkan");
    return TRUE;
  }
  public  function deletesaran($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('saran');
    $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
    return TRUE;
  }
  public function editsaran($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('saran',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }

	public function getprofile(){
		$queryprofile = $this->db->query('SELECT * FROM proflie_robonesia');
		return $queryprofile; 
	}

	public function getartikel(){
		$queryartikel = $this->db->query('SELECT * FROM artikel');
		return $queryartikel;
	}

  public function getfoto(){
    $queryfoto = $this->db->query('SELECT * FROM galeri_foto');
    return $queryfoto;
  }

  public function getuser(){
    $queryuser = $this->db->query('SELECT * FROM user');
    return $queryuser;
  }

  public function insertfoto($data){
    $this->db->insert('galeri_foto',$data);
    $this->session->set_flashdata('sukses',"Berita Berhasil Ditambahkan");
    return TRUE;
  }

  public function editfoto($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('galeri_foto',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }

  public  function deletefoto($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('galeri_foto');
    $this->session->set_flashdata('sukses',"Artikel Berhasil Dihapus");
    return TRUE;
  }

  public function insertartikel($data){
    $this->db->insert('artikel',$data);
    $this->session->set_flashdata('sukses',"Berita Berhasil Ditambahkan");
    return TRUE;
  }
  public  function deleteartikel($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('artikel');
    $this->session->set_flashdata('sukses',"Artikel Berhasil Dihapus");
    return TRUE;
  }
  public function editartikel($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('artikel',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public function getkaryawan(){
    $querykaryawan = $this->db->query('SELECT * FROM karyawan');
    return $querykaryawan;
  }
  public function insertkaryawan($data){
    $this->db->insert('karyawan',$data);
    $this->session->set_flashdata('sukses',"Berita Berhasil Ditambahkan");
    return TRUE;
  }
  public  function deletekaryawan($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('karyawan');
    $this->session->set_flashdata('sukses',"Artikel Berhasil Dihapus");
    return TRUE;
  }
  public function editkaryawan($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('karyawan',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public function getmember(){
    $querymember = $this->db->query('SELECT * FROM member');
    return $querymember;
  }
  public function insertmember($data){
    $this->db->insert('member',$data);
    $this->session->set_flashdata('sukses',"Berita Berhasil Ditambahkan");
    return TRUE;
  }
  public function editmember($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('member',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public  function deletemember($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('member');
    $this->session->set_flashdata('sukses',"Artikel Berhasil Dihapus");
    return TRUE;
  }
  public function getseminar(){
    $queryseminar = $this->db->query('SELECT * FROM seminar');
    return $queryseminar;
  }
  public function insertseminar($data){
    $this->db->insert('seminar',$data);
    $this->session->set_flashdata('sukses',"Berita Berhasil Ditambahkan");
    return TRUE;
  }
  public function editseminar($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('seminar',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }
  public  function deleteseminar($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('seminar');
    $this->session->set_flashdata('sukses',"Artikel Berhasil Dihapus");
    return TRUE;
  }
  public function getvideo(){
    $queryvideo = $this->db->query('SELECT * FROM galeri_video');
    return $queryvideo;
  }

  public function insertvideo($data){
    $this->db->insert('galeri_video',$data);
    $this->session->set_flashdata('sukses',"Berita Berhasil Ditambahkan");
    return TRUE;
  }

  public function editvideo($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('galeri_video',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }

  public  function deletevideo($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('galeri_video');
    $this->session->set_flashdata('sukses',"Artikel Berhasil Dihapus");
    return TRUE;
  }

  public function insertuser($data){
    $this->db->insert('user',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Ditambahkan");
    return TRUE;
  }

  public function edituser($data,$id){
    $this->db->where('id', $_POST['id']);
    $this->db->update('user',$data);
    $this->session->set_flashdata('sukses',"Data Berhasil Diedit");
    return TRUE;
  }

  public  function deleteuser($data, $id){
    $this->db->where('id', $id);
    $this->db->delete('user');
    $this->session->set_flashdata('sukses',"Data Berhasil Dihapus");
    return TRUE;
  }

  public function getlengthuser(){
    $query = $this->db->query('SELECT COUNT(*) FROM karyawan');
    return $query;
  }

  public function getlengthsaran(){
    $query = $this->db->query('SELECT COUNT(*) FROM saran');
    return $query;
  }

  public function getlengthartikel(){
    $query = $this->db->query('SELECT COUNT(*) FROM artikel');
    return $query;
  }

  public function getlengthfoto(){
    $query = $this->db->query('SELECT COUNT(*) FROM galeri_foto');
    return $query;
  }

}